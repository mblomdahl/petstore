
===============
 PetStore Docs
===============

.. toctree::
   :maxdepth: 2

.. automodule:: manage
   :platform: Unix, Windows
   :synopsis: Command-line interface for the application.
   :members:

.. automodule:: wsgi
   :platform: Unix, Windows
   :synopsis: Bootstrapping for the web server and :term:`WSGI` app.
   :members:

.. automodule:: petstore
   :platform: Unix, Windows
   :synopsis: Root :mod:`petstore` application package.
   :members:

   .. automodule:: petstore.api
      :platform: Unix, Windows
      :synopsis: API application package.

      .. autofunction:: petstore.api.route

      .. automodule:: petstore.api.pet
         :platform: Unix, Windows
         :synopsis: Pet management API endpoints.
         :members:

         .. autofunction:: petstore.api.pet.post_pet

         .. autofunction:: petstore.api.pet.get_pet

         .. autofunction:: petstore.api.pet.list_pets

         .. autofunction:: petstore.api.pet.patch_pet

         .. autofunction:: petstore.api.pet.delete_pet

   .. automodule:: petstore.manage
      :platform: Unix, Windows
      :synopsis: Manager package.
      :members: pet_manager, database_manager

      .. automodule:: petstore.manage.pet
         :platform: Unix, Windows
         :synopsis: Pet management commands.
         :members: create_pet, list_pets, delete_pet

      .. automodule:: petstore.manage.database
         :platform: Unix, Windows
         :synopsis: Database management commands.
         :members: init_db, drop_db

   .. automodule:: petstore.models
      :platform: Unix, Windows
      :synopsis: Models module.
      :members:

   .. automodule:: petstore.settings
      :platform: Unix, Windows
      :synopsis: Settings module.
      :members:

   .. automodule:: petstore.core
      :platform: Unix, Windows
      :synopsis: Core module.
      :members:

   .. automodule:: petstore.services
      :platform: Unix, Windows
      :synopsis: Services module.
      :members:

   .. automodule:: petstore.factory
      :platform: Unix, Windows
      :synopsis: Factory module.
      :members:



====================
 Indices and tables
====================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

