# -*- coding: utf-8 -*-
#
# Title:   tests.py
# Author:  Mats Blomdahl
# Version: 2014-05-14

import json
import httplib
import pprint


conn = httplib.HTTPConnection('127.0.0.1:5000')


def _test_create():

    print '###### TEST CREATE PET (POST /petstore/pet) ###### ###### ######'

    url = '/petstore/pet'

    pet = {
        'name': 'Fido',
        'kind': 'cat'
    }

    conn.request('POST', url, json.dumps(pet))
    response = json.loads(conn.getresponse().read())
    pprint.pprint(response)
    assert response['success'] is True
    data = response['retval']
    assert 'id' in data
    assert data['name'] == 'Fido'
    assert data['kind'] == 'cat'
    assert data['age'] is None

    print '\n[ OK ]\n\n\n\n'
    return data

test_pet = _test_create()


def _test_get(test_pet):

    print '###### TEST RETRIEVE (GET /petstore/pet/<id>) ###### ###### ######'

    url = '/petstore/pet/%s' % test_pet['id']

    conn.request('GET', url)
    response = json.loads(conn.getresponse().read())
    pprint.pprint(response)
    
    assert response['success'] is True
    data = response['retval']
    assert 'id' in data
    assert data['name'] == 'Fido'
    assert data['kind'] == 'cat'
    assert data['age'] is None

    print '\n[ OK ]\n\n\n\n'
    return data

test_pet = _test_get(test_pet)


def _test_list(test_pet):

    print '###### TEST LIST PETS (GET /petstore/pet) ###### ###### ######'

    url = '/petstore/pet'

    conn.request('GET', url)
    response = json.loads(conn.getresponse().read())
    pprint.pprint(response)

    assert response['success'] is True
    data = response['retval']
    assert isinstance(data, list)
    for pet in data:
        if pet['id'] == test_pet['id']:
            break
    else:
        raise AssertionError('didn\'t find test pet in list output')

    print '\n[ OK ]\n\n\n\n'
    return data

all_pets = _test_list(test_pet)


def _test_update(test_pet):

    print '###### TEST UPDATE (PATCH /petstore/pet/<id>) ###### ###### ######'

    url = '/petstore/pet/%s' % test_pet['id']

    pet_updates = {
        'name': 'Fido-mod',
        'kind': 'micro-cat',
        'age': None
    }

    conn.request('PATCH', url, json.dumps(pet_updates))
    response = json.loads(conn.getresponse().read())
    pprint.pprint(response)
    
    assert response['success'] is True
    data = response['retval']
    assert data['id'] == test_pet['id']
    assert data['name'] == 'Fido-mod'
    assert data['kind'] == 'micro-cat'
    assert data['age'] == None

    print '\n[ OK ]\n\n\n\n'
    return data

mod_pet = _test_update(test_pet)


def _test_delete(test_pet):

    print '###### TEST DELETE (DELETE /petstore/pet/<id>) ###### ###### ######'

    url = '/petstore/pet/%s' % test_pet['id']

    conn.request('DELETE', url)
    response = json.loads(conn.getresponse().read())
    pprint.pprint(response)
    
    assert response['success'] is True
    data = response['retval']
    assert data['id'] == test_pet['id']
    assert data['name'] == test_pet['name']
    assert data['kind'] == test_pet['kind']
    assert data['age'] == test_pet['age']

    print '\n[ OK ]\n\n\n\n'
    return data

dead_pet = _test_delete(mod_pet)

