# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.factory`
    ``````````````````````````

    Factory module.

    .. seealso:: :py:mod:`petstore.settings`,
                 :py:mod:`petstore.core`,
                 :py:mod:`petstore.api.pet`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['create_app']

from flask import Flask

from .api.pet import pet_api_bp
from .core import db


def create_app(config_filename):
    u""".. versionadded:: 0.1-rc1

    Create a :py:class:`flask.Flask` app and initialize with
    :py:class:`~flask.ext.sqlalchemy.SQLAlchemy` database.

    :param str config_filename: Path to a Python config file.
    :returns: A WSGI app.
    """

    app = Flask(__name__)
    app.config.from_pyfile(config_filename)

    db.init_app(app)

    app.register_blueprint(pet_api_bp)

    return app
