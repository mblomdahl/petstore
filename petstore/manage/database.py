# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.manage.database`
    ``````````````````````````````````

    Database management commands.

    .. seealso:: :py:mod:`petstore.core`,
                 :py:class:`petstore.models.Pet`,
                 :py:mod:`manage`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['database_manager']

from flask.ext.script import Manager, prompt_bool

from ..core import db


database_manager = Manager(with_default_commands=False,
                           help='Database management command-line tool.')

@database_manager.command
def init_db():
    u"""Initializes the sqlite3 database."""

    db.create_all(bind=['pets'])
    print '\n[INFO] sqlite3 storage backend configured.\n'


@database_manager.command
def drop_db():
    u"""Drops all tables in the sqlite3 database."""

    if prompt_bool('[WARN] This will destroy any data stored in the database. '
                   'Are you really really sure you want to drop all tables?'):

        db.drop_all(bind=['pets'])
        print '\n[INFO] All sqlite3 tables dropped.\n'
    else:
        print '\n[INFO] Operation cancelled. Nothing to do.\n'


