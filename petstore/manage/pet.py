# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.manage.pet`
    `````````````````````````````

    Pet management commands.

    .. seealso:: :py:mod:`petstore.services`,
                 :py:class:`petstore.models.Pet`,
                 :py:mod:`manage`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['pet_manager']

from flask.ext.script import Manager
from flask.ext.script.commands import InvalidCommand

from ..services import pets


_PET_STR_FM = '  Pet(id={id}, name={name!r}, kind={kind!r}, age={age})'


pet_manager = Manager(with_default_commands=False,
                      help='Pet management command-line tool.')


@pet_manager.option('age', type=int, help='Current age.')
@pet_manager.option('kind', help='Species/breed.')
@pet_manager.option('name', help='Friendly name.')
def create_pet(**kwargs):
    u"""Creates a new Pet record 'name kind age'."""

    pet_instance = pets.create(**kwargs)
    print '[INFO] Pet record created:'
    print _PET_STR_FM.format(**pet_instance.to_dict())
    print '\n'

@pet_manager.command
def list_pets():
    u"""Lists all Pet records."""

    print '\n[INFO] Pet records:'
    for pet_instance in pets.all():
        print _PET_STR_FM.format(**pet_instance.to_dict())
    print '\n'

@pet_manager.option('pet_id', type=int, help='Pet record ID.')
def delete_pet(pet_id=None):
    u"""Deletes a Pet record."""

    pet_instance = pets.get(pet_id)
    if not pet_instance:
        raise InvalidCommand('Unknown pet ID {!r}.'.format(pet_id))

    pets.delete(pet_instance)
    print '\n[INFO] Pet record deleted:'
    print _PET_STR_FM.format(**pet_instance.to_dict())
    print '\n'
