# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.manage`
    -------------------------

    Manager package.

    .. seealso:: :py:mod:`petstore.manage.pet`,
                 :py:mod:`petstore.manage.database`,
                 :py:mod:`manage`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['pet_manager', 'database_manager']

from .pet import pet_manager
from .database import database_manager
