# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.settings`
    ```````````````````````````

    Settings module.

    .. seealso:: :py:mod:`petstore.api`,
                 :py:mod:`petstore.core`,
                 :py:mod:`petstore.models`,
                 :py:func:`petstore.factory.create_app`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

import datetime
import os

DEBUG = True
SERVER_NAME = '127.0.0.1:5000'
LOGGER_NAME = 'petstore'
APPLICATION_ROOT = '/'

SQLITE3_DATABASE_ROOT = 'petstore2.db'
SQLALCHEMY_DATABASE_URI = 'sqlite://'
SQLALCHEMY_BINDS = {
    'pets': 'sqlite:///{0}'.format(os.path.abspath(SQLITE3_DATABASE_ROOT))}

CELERY_BROKER_URL = 'redis://127.0.0.1:6379/0'

SESSION_COOKIE_PATH = '/'
SESSION_COOKIE_DOMAIN = None
SESSION_COOKIE_NAME = 'petstore_session'
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = False
SESSION_REFRESH_EACH_REQUEST = True
SECRET_KEY = 'super-secret-key'
PERMANENT_SESSION_LIFETIME = datetime.timedelta(31)

SEND_FILE_MAX_AGE_DEFAULT = 43200
PREFERRED_URL_SCHEME = 'http'
PROPAGATE_EXCEPTIONS = True
PRESERVE_CONTEXT_ON_EXCEPTION = None
TRAP_HTTP_EXCEPTIONS = False
