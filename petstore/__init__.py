# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore`
    ==================

    Root application package.

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""
