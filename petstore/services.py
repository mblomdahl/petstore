# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.services`
    ```````````````````````````

    Services module.

    .. seealso:: :py:mod:`petstore.models`,
                 :py:class:`petstore.models.Pet`,
                 :py:mod:`petstore.api.pet`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['pets']

from .models import Pet
from .core import Service


class _PetService(Service):
    __model__ = Pet


#: :py:obj:`~pets` service based on :py:class:`petstore.core.Service`.
pets = _PetService()
