# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.core`
    ```````````````````````

    Core module.


    :py:class:`petstore.core.Service`
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Effortlessly stolen from Matt Wright's `Overholt application
    <https://github.com/mattupstate/overholt/blob/master/overholt/core.py`_.

    .. moduleauthor:: Matt Wright
"""

__all__ = ['Service']

from flask_sqlalchemy import SQLAlchemy

#: :py:class:`flask.ext.sqlalchemy.SQLAlchemy` extension instance.
db = SQLAlchemy()


class Service(object):
    u"""A :class:`Service` instance encapsulates common
    :py:class:`~flask.ext.sqlalchemy.SQLAlchemy` model operations in the context
    of a :py:class:`~flask.Flask` application.
    """
    __model__ = None

    def _isinstance(self, model, raise_error=True):
        u"""Checks if the specified model instance matches the service's model.
        By default this method will raise a `ValueError` if the model is not the
        expected type.

        :param model: the model instance to check
        :param raise_error: flag to raise an error on a mismatch
        """

        rv = isinstance(model, self.__model__)
        if not rv and raise_error:
            raise ValueError('%s is not of type %s' % (model, self.__model__))
        return rv

    def _preprocess_params(self, kwargs):
        u"""Returns a preprocessed dictionary of parameters. Used by default
        before creating a new instance or updating an existing instance.

        :param kwargs: a dictionary of parameters
        """

        kwargs.pop('csrf_token', None)
        return kwargs

    def save(self, model):
        u"""Commits the model to the database and returns the model

        :param model: the model to save
        """

        self._isinstance(model)
        db.session.add(model)
        db.session.commit()
        return model

    def all(self):
        u"""Returns a generator containing all instances of the service's model.
        """

        return self.__model__.query.all()

    def get(self, id):
        u"""Returns an instance of the service's model with the specified id.
        Returns `None` if an instance with the specified id does not exist.

        :param id: the instance id
        """

        return self.__model__.query.get(id)

    def get_all(self, *ids):
        u"""Returns a list of instances of the service's model with the specified
        ids.

        :param ids: instance ids
        """

        return self.__model__.query.filter(self.__model__.id.in_(ids)).all()

    def find(self, **kwargs):
        u"""Returns a list of instances of the service's model filtered by the
        specified key word arguments.

        :param kwargs: filter parameters
        """

        return self.__model__.query.filter_by(**kwargs)

    def first(self, **kwargs):
        u"""Returns the first instance found of the service's model filtered by
        the specified key word arguments.

        :param kwargs: filter parameters
        """

        return self.find(**kwargs).first()

    def get_or_404(self, id):
        u"""Returns an instance of the service's model with the specified id or
        raises an 404 error if an instance with the specified id does not exist.

        :param id: the instance id
        """

        return self.__model__.query.get_or_404(id)

    def new(self, **kwargs):
        u"""Returns a new, unsaved instance of the service's model class.

        :param kwargs: instance parameters
        """
        return self.__model__(**self._preprocess_params(kwargs))

    def create(self, **kwargs):
        u"""Returns a new, saved instance of the service's model class.

        :param kwargs: instance parameters
        """

        return self.save(self.new(**kwargs))

    def update(self, model, **kwargs):
        u"""Returns an updated instance of the service's model class.

        :param model: the model to update
        :param kwargs: update parameters
        """

        self._isinstance(model)
        for k, v in self._preprocess_params(kwargs).items():
            setattr(model, k, v)
        self.save(model)
        return model

    def delete(self, model):
        u"""Immediately deletes the specified model instance.

        :param model: the model instance to delete
        """

        self._isinstance(model)
        db.session.delete(model)
        db.session.commit()
