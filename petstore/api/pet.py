# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.api.pet`
    ``````````````````````````

    Pet management utility JSON-API endpoints.

    .. seealso:: :py:mod:`petstore.factory`,
                 :py:mod:`petstore.services`,
                 :py:class:`petstore.models.Pet`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['pet_api_bp']

from flask import current_app, request, Blueprint

from ..services import pets
from . import route


#: Root :py:class:`flask.Blueprint` API interface.
pet_api_bp = Blueprint('pet_api_bp', __name__, url_prefix='/petstore/pet')


@route(pet_api_bp, '', methods=['POST'])
def post_pet():
    u""".. versionadded:: 0.1-alpha

    .. http:post:: /petstore/pet

        Create a new from the JSON payload.

        **Example request:**

        .. sourcecode:: http

            POST /petstore/pet/2 HTTP/1.1
            Host: example.com
            Accept: application/json
            Content-Type: application/json

            {
                "name": "Fido",
                "kind": "cat"
            }

        **Example response:**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Vary: Accept
            Content-Type: application/json

            {
                "success": true,
                "retval": {
                    "id": 3,
                    "name": "Fido",
                    "kind": "cat",
                    "age": null
                }
            }

        :json string body: A JSON object with the new fields and values.
        :status 200: No errors.
        :returns: :py:const:`None`
    """

    task_data = request.get_json(force=True)
    new_pet = pets.create(**task_data).to_dict()
    current_app.logger.info(
        '[petstore.api.pet.post_pet] Pet created: \n\t\'%s\'' % str(new_pet))
    return new_pet


@route(pet_api_bp, '/<int:pet_id>', methods=['GET'])
def get_pet(pet_id):
    u""".. versionadded:: 0.1-alpha

    .. http:get:: /petstore/pet/(int:pet_id)

        Retrieve the pet referenced by `pet_id`.

        **Example request:**

        .. sourcecode:: http

            GET /petstore/pet/2 HTTP/1.1
            Host: example.com
            Accept: application/json

        **Example response:**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Vary: Accept
            Content-Type: application/json

            {
                "success": true,
                "retval": {
                    "id": 2,
                    "name": "Bruno",
                    "kind": "dog",
                    "age": 3
                }
            }

        :param int pet_id: A pet ID.
        :status 200: No errors.
        :status 404: No match for `pet_id`.
        :returns: :py:const:`None`
    """

    pet_instance = pets.get(pet_id)
    if not pet_instance:
        current_app.logger.error(
            '[get_pet] Pet retrieval failed; unknown pet ID \'%i\'.' % pet_id)
        return 'Unknown pet ID \'%i\'' % pet_id, 404
    else:
        current_app.logger.info(
            '[get_pet] Pet record retrieved: \n\t\'%s\'' % str(pet_instance))
        return pet_instance.to_dict()


@route(pet_api_bp, '', methods=['GET'])
def list_pets():
    u""".. versionadded:: 0.1-alpha

    .. http:get:: /petstore/pet

        Retrieve a list of all pets stored in the local :py:mod:`sqlite3`
        database.

        **Example request:**

        .. sourcecode:: http

            GET /petstore/pet HTTP/1.1
            Host: example.com
            Accept: application/json

        **Example response:**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Vary: Accept
            Content-Type: application/json

            {
                "success": true,
                "data": [
                    {
                        "id": 1
                        "name": "Luffy",
                        "kind": "rodent",
                        "age": 1
                    },
                    {
                        "id": 2
                        "name": "Bruno",
                        "kind": "dog",
                        "age": 3
                    },
                    {
                        "id": 3
                        "name": "Fido",
                        "kind": "cat",
                        "age": null
                    }
                ]
            }

        :status 200: No errors.
        :returns: :py:const:`None`
    """

    pets_list = [pet_instance.to_dict() for pet_instance in pets.all()]
    current_app.logger.info('[list_pets] Pet records retrieved:\n\t\'%s\''
                            % str(pets_list))
    return pets_list


@route(pet_api_bp, '/<int:pet_id>', methods=['PATCH'])
def patch_pet(pet_id):
    u""".. versionadded:: 0.1-beta

    .. http:patch:: /petstore/pet/(int:pet_id)

        Partially update a pet referenced by `pet_id` and respond with the
        full pet record.

        **Example request:**

        .. sourcecode:: http

            PATCH /petstore/pet/2 HTTP/1.1
            Host: example.com
            Accept: application/json
            Content-Type: application/json

            {
                "name": "Bruno the dog",
                "kind": "dalmatian"
            }

        **Example response:**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Vary: Accept
            Content-Type: application/json

            {
                "success": true,
                "retval": {
                    "id": 2,
                    "name": "Bruno the dog",
                    "kind": "dalmatian",
                    "age": 3
                }
            }

        :param int pet_id: A pet ID.
        :json string body: A serialized JSON object with the updated
                           fields/values.
        :status 200: No errors.
        :status 404: No match for `pet_id`.
        :returns: :py:const:`None`
    """

    task_data = request.get_json(force=True)

    pet_instance = pets.get(pet_id)
    if not pet_instance:
        current_app.logger.error(
            '[patch_pet] Record update failed; unknown pet ID \'%i\'.' % pet_id)
        return 'Unknown pet ID \'%i\'.' % pet_id, 404
    else:
        updated_pet = pets.update(pet_instance, **task_data).to_dict()
        current_app.logger.info('[patch_pet] Pet record updated:\n\t\'\%s\''
                                % updated_pet)
        return updated_pet


@route(pet_api_bp, '/<int:pet_id>', methods=['DELETE'])
def delete_pet(pet_id):
    u""".. versionadded:: 0.1-beta

    .. http:delete:: /petstore/pet/(int:pet_id)

        Delete the pet referenced by `pet_id` and respond with the deleted
        record.

        **Example request:**

        .. sourcecode:: http

            DELETE /petstore/pet/2 HTTP/1.1
            Host: example.com
            Accept: application/json

        **Example response:**

        .. sourcecode:: http

            HTTP/1.1 200 OK
            Vary: Accept
            Content-Type: application/json

            {
                "success": true,
                "retval": {
                    "id": 2,
                    "name": "Bruno the dog",
                    "kind": "dalmatian",
                    "age": 3
                }
            }

        :param int pet_id: A pet ID.
        :status 200: No errors.
        :status 404: No match for `pet_id`.
        :returns: :py:const:`None`
    """

    pet_instance = pets.get(pet_id)
    if not pet_instance:
        current_app.logger.error(
            '[delete_pet] Deletion failed; unknown pet ID \'%i\'.' % pet_id)
        return 'Unknown pet ID \'%i\'.' % pet_id, 404
    else:
        pets.delete(pet_instance)
        current_app.logger.info('[delete_pet] Pet record deleted:\n\t\'%s\''
                                % pet_instance.to_dict())
    return pet_instance.to_dict()

