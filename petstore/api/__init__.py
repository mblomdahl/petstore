# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.api`
    ----------------------

    API application package.

    .. seealso:: :py:mod:`petstore.api.pet`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['route']

from functools import wraps

from flask import jsonify


def route(blueprint, *args, **kwargs):
    u""".. versionadded: 0.1-rc1

    Decorates API endpoints to provide uniform output.

    :param blueprint: Parent :py:class:`flask.Blueprint` instance.
    """

    def _decorator(fn):
        @blueprint.route(*args, **kwargs)
        @wraps(fn)
        def wrapper(*args, **kwargs):
            u""".. versionadded: 0.1-rc1

            Wrap response in a dict and serialize the handler response.
            """

            retval = fn(*args, **kwargs)
            if isinstance(retval, tuple):
                retval, status = retval
                return jsonify({'success': False, 'reason': retval}), status
            else:
                return jsonify({'success': True, 'retval': retval})

        return fn

    return _decorator
