# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.models`
    -------------------------

    Models module.

    .. seealso:: :py:mod:`petstore.services`
                 :py:class:`petstore.models.Pet`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['Pet']

from .pet import Pet
