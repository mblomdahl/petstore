# -*- coding: utf-8 -*-
u"""
    :py:mod:`petstore.models.pet`
    `````````````````````````````

    :py:class:`petstore.models.Pet` model.

    .. seealso:: :py:mod:`petstore.models`
                 :py:mod:`petstore.services`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['Pet']

from ..core import db


class Pet(db.Model):
    u""".. versionadded:: 0.1-rc1

    Custom pet representation using :py:class:`flask_sqlalchemy.Model`.
    """
    __bind_key__ = 'pets'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    kind = db.Column(db.String(), nullable=False)
    age = db.Column(db.Integer(), nullable=True)

    def __init__(self, name=None, kind=None, age=None):
        self.name, self.kind, self.age = name, kind, age

    def __repr__(self):
        return '<Pet %r>' % self.id

    def to_dict(self):
        u""".. versionadded:: 0.1-rc1

        Return a :py:class:`dict` with the instance's public properties.

        :returns: :py:class:`dict`
        """

        return {
            'id': self.id,
            'name': self.name,
            'kind': self.kind,
            'age': self.age
        }
