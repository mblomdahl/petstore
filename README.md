# PetStore API

## 1. Description

Tiny API implementation using the [Flask](http://flask.pocoo.org/) framework,
[sqlite3](https://docs.python.org/2/library/sqlite3.html),
[SQLAlchemy](http://www.sqlalchemy.org/) and [Sphinx](http://sphinx-doc.org/).

The overall application structure, design patterns, code style and most about
everything is stolen directly from Matt Wright's excellent article
[How I Structure My Flask Applications](http://mattupstate.com/python/2013/06/26/how-i-structure-my-flask-applications.html)
and the associated [Overholt](https://github.com/mattupstate/overholt) repo.
Though there's still a long way to go before we're on par with Wright's
solution. ;-)


## 2. Use Cases / Applications

Keeping track of your pets' names, species and age.


## 3. API Interface

### 3.1. Creating a New Pet Record

Endpoint:

    POST /petstore/pet


Request body: A JSON object containing name (required), age and kind (optional).
Example payload:

    {
        "name": "Fido",
        "kind": "cat"
    }


Response: The created pet record. Example:

    {
        "success": true,
        "retval": {
            "id": 3
            "name": "Fido",
            "kind": "cat",
            "age": null
        }
    }


### 3.2. Listing Pet Records

Endpoint:

    GET /petstore/pet


Response: A JSON object containing an array of pet records. Example:

    {
        "success": true,
        "retval": [
            {
                "id": 1
                "name": "Luffy",
                "kind": "rodent",
                "age": 1
            },
            {
                "id": 2
                "name": "Bruno",
                "kind": "dog",
                "age": 3
            },
            {
                "id": 3
                "name": "Fido",
                "kind": "cat",
                "age": null
            }
        ]
    }


### 3.3. Retrieving a Pet Record

Endpoint:

    GET /petstore/pet/<pet_id>


Response: The pet record referenced by `pet_id`. Example:

    {
        "success": true,
        "retval": {
            "id": 2,
            "name": "Bruno",
            "kind": "dog",
            "age": 3
        }
    }


### 3.4. Updating a Pet Record

Endpoint:

    PATCH /petstore/pet/<pet_id>


Request body: A JSON object containing the updated field values for the pet
record referenced by `pet_id`. Example payload:

    {
        "name": "Bruno the dog",
        "kind": "dalmatian"
    }


Response: The updated pet record. Example:

    {
        "success": true,
        "retval": {
            "id": 2
            "name": "Bruno the dog",
            "kind": "dalmatian",
            "age": 3
        }
    }


### 3.5. Deleting a Pet Record

Endpoint:

    DELETE /petstore/pet/<pet_id>


Response: A JSON object containing the deleted pet record. Example:

    {
        "success": true,
        "retval": {
            "id": 2
            "name": "Bruno the dog",
            "kind": "dalmatian",
            "age": 3
        }
    }


## 4. Known Issues

* Security haven't been a top priority (does Flask-SQLAlchemy sanitize input
  data to prevent SQL injection attacks?)
* No instructive / user-friendly error handling.
* Validation forms is yet to be implemented.


## 5. Setup

Interpreter: Python 2.7.6

Required Python packages:

* [Flask](http://webapp-improved.appspot.com/index.html)
* ... and many others (refer to the *Roadmap* heading for plans ahead).


### 5.1. Running the API


1. Clone repo into dev root:

        $ cd ~/my_dev_root
        $ git clone git@bitbucket.org:mblomdahl/petstore.git
        $ cd petstore


2. Initial setup of the `sqlite3` database:

        $ python manage.py database init_db


3. Start the HTTP server:

        $ python wsgi.py


4. Checkout the Sphinx API docs:

        GET 127.0.0.1:5000/docs/index.html


5. Try adding some pets by sending a HTTP-POST requests to
   `127.0.0.1:5000/petstore/pet` with new animals.


6. Confirm the pets have been registered as expected:

        $ python manage.py petstore list_pets


## 6. Roadmap

Upcoming release `0.1-rc2`:

* Implement the Celery taskqueue /w Redis backend + exchange.

* Extend CLI functionality + add meaningful docs.

* Implement SOCKSv5 connector for Tor.

* Implement form validations for the API and CLI interfaces using Flask
  extensions.

* Add list of requirements to resolve dependencies using

        $ pip install -r requirements.txt

* Implement `setup.py` installation script.

