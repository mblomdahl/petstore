# -*- coding: utf-8 -*-
u"""
    :py:mod:`manage`
    ````````````````

    Manager CLI module.

    .. seealso:: :py:mod:`petstore.manage`

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

from flask.ext.script import Manager

from petstore.factory import create_app
from petstore.manage import pet_manager, database_manager


base_manager = Manager(create_app('settings.py'), with_default_commands=False)

base_manager.add_command('petstore', pet_manager)
base_manager.add_command('database', database_manager)


if __name__ == '__main__':
    base_manager.run()
