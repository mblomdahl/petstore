# -*- coding: utf-8 -*-
u"""
    :py:mod:`wsgi`
    ``````````````

    WSGI bootstrapper module.

    .. moduleauthor:: Mats Blomdahl <mats.blomdahl@gmail.com>
"""

__all__ = ['run_server']

import os

from werkzeug.wsgi import DispatcherMiddleware
from werkzeug.serving import run_simple

from petstore.factory import create_app


def run_server(settings_file='settings.py', host='127.0.0.1', port=5000):
    u""".. versionadded:: 0.1-rc1

    Create WSGI app using the :py:func:`petstore.factory.create_app` application
    factory and start the web server on `host`:`port`.

    :param str settings_file: Application settings path.
    :param str host: Host IP address.
    :param int port: Host port number.
    :returns: :py:const:`None`
    """

    app = DispatcherMiddleware(create_app(settings_file))

    static_docs = {'/docs': os.path.join(os.path.dirname(__file__),
                                         'docs', 'build', 'html')}

    run_simple(host, port, app, use_reloader=True, static_files=static_docs)

if __name__ == '__main__':
    run_server()
